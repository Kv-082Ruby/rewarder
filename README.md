# Rewarder

Rewarder will help you to reward your employees in fast, comfortable and easy way.

- ruby: 3.0.1
- rails: 6.1.3.2
- database: postgresql
- mode: API
- test suite: RSpec

## Test run of the initial project stage

1. Clone repository locally.
2. Run `bundle install`.
3. Locally, create postgres role for the project with a password and username.
4. Locally, in the root of the app create three files: ".env.development", ".env.test", ".env.production". Make sure you have the preceding dots and dots after "env"! 
5. Write down credentials for access to your local postgres database for each environment in these files (production could be skipped for now). Make sure names of environmental variables match names in /config/database.yml file. Example of ".env.development" file: 
```
DATABASE_HOST=localhost
DATABASE_USERNAME=your_db_username
DATABASE_PASSWORD=your_db_password
```
6. In CLI, run `rake db:setup`.
7. In CLI, run `rails s` and head to "http://localhost:3000/" in your browser. You should be able to see pretty standard Rails greeting "Yay! You're on Rails". 
8. Make sure all ".env" files are in .gitignore before pushing your changes to remote. 

