class CreateOrganizations < ActiveRecord::Migration[6.1]
  def change
    create_table :organizations do |t|
      t.string :name
      t.string :picture_url
      t.string :invitation_link
      t.string :domain
      t.boolean :is_public, default: false

      t.timestamps
    end

    add_index :organizations, [:name, :invitation_link], unique: true
  end
end
