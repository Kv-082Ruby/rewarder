class Organization < ApplicationRecord
  validates_presence_of :name, :invitation_link, :domain
  validates_uniqueness_of :name, :invitation_link
end
